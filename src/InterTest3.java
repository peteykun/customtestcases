class Container {
	Object f;
}


public class InterTest3 {
	public static void source(Container c) {
		c.f = new Object();
	}
	
	public static void sink(Container c) {
		
	}
	
	public static void fetchAndPut() {
		Container obj = new Container();
		/* There will be 2 call strings here */
		source(obj);
		sink(obj);
	}
	
	public static void main(String[] args) {
		fetchAndPut();
		fetchAndPut();
	}
}
