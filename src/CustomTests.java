import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

interface FooFace {
	
}

class Foo implements FooFace {
	public static void bar() {
		String s = new String("Foo:bar");
		// System.out.println(s);
	}
	
	public static Object source() {
		return new Object();
	}
	
	public static void sink(Object o) {
		o.hashCode();
	}
}

class Foooo extends Foo {
	public static void baz() {
		String s = new String("Foooo:baz");
		// System.out.println(s);
	}
	
	public static void bar() {
		String s = new String("Foooo:bar");
		// System.out.println(s);
	}
}

public class CustomTests {
	CustomTests f;
	static CustomTests g;
	
	@SuppressWarnings("static-access")
	public static void simpleCall() {
		Foo foo = new Foo();
		Foooo foooo = new Foooo();
		
		foo.bar();
		foooo.bar();
	}
	
	public static void simpleCall2() {
		Foo.bar();
		Foooo.bar();
	}
	
	public static void sourceSink() {
		Object o = Foo.source();
		Foo.sink(o);
	}
	
	public static void sourceSink2() {
		Object o = Foooo.source();
		Foooo.sink(o);
	}
	
	@SuppressWarnings("static-access")
	public static void interfaceTest() {
		FooFace foo = new Foo();
		
		// Cannot do this!
		// foo.bar();
		
		// Have to do this
		((Foo) foo).bar();		// Foo:bar
	}
	
	@SuppressWarnings("static-access")
	public static void subclassTest() {
		Foo foooo = new Foooo();
		
		// Cannot do this!
		// foooo.baz();
		
		// Have to do this
		((Foooo) foooo).baz();	// Foooo:baz
	}
	
	@SuppressWarnings("static-access")
	public static void subclassTest2() {
		Foo foooo = new Foooo();
		
		foooo.bar();			// Foo:bar (no late binding!)
		((Foooo) foooo).bar();	// Foooo:bar
	}
	
	public static CustomTests fetch_new() {
		return new CustomTests();
	}
	
	public static int[] fetch_primitive_array(int n) {
		int[] numbers = new int[5];
		
		for (int i = 0; i < n; i++)
			numbers[i] = i + 1;
		
		return numbers;
	}
	
	public static String[] fetch_String_array(int n) {
		String[] numbers = new String[5];
		
		for (int i = 0; i < n; i++)
			numbers[i] = Integer.toString(i + 1);
		
		return numbers;
	}
	
	public static void main(String[] args) {
		/* testNew();
		testInvoke();
		testPut1();
		testPut2();
		testPhi1(5);
		testPhi2(5);
		testPhi3(5);
		testPhi4(5);
		testGet1();
		testGet2();
		testNull();
		testNull2();
		testNull3(true);
		testNull4();
		testNull5();
		testPutStatic();
		testGetStatic();
		testCond1(1, 2);
		testCond2(1, 2);
		testCond3(1, 2);
		testCond4(1, 2);
		testCast(); */
		
		/* Phase 2 */
		simpleCall();
		simpleCall2();
		interfaceTest();
		subclassTest();
		subclassTest2();
		sourceSink();
		sourceSink2();
	}
	
	// PASS
	@SuppressWarnings("unused")
	public static void testNew() {
		// Primitives
		boolean bool = false;
		byte byt = 0;
		char ch = 'A';
		short smol = 0;
		int n = 0;
		long l = 0L;
		float f = 0.0f;
		double d = 0.0d;
		
		// Reference types
		Object obj = new Object();
		String str = new String();
		
		// Primitive arrays
		int[] numbers = new int[5];
		
		for (int i = 0; i < n; i++)
			numbers[i] = i + 1;
		
		// Reference arrays
		String[] strings = new String[5];
		
		for (int i = 0; i < n; i++)
			strings[i] = Integer.toString(i + 1);
	}
	
	// PASS
	@SuppressWarnings("unused")
	public static void testInvoke() {
		Scanner sc = new Scanner(System.in);
		
		// Primitives
		boolean bool = sc.nextBoolean();
		byte byt = sc.nextByte();
		short smol = sc.nextShort();
		int n = sc.nextInt();
		long l = sc.nextLong();
		float f = sc.nextFloat();
		double d = sc.nextDouble();
		
		// Reference types
		Object obj = fetch_new();
		String str = sc.next();
		
		// Primitive arrays
		int[] numbers = fetch_primitive_array(5);
		
		// Reference arrays
		String[] strings = fetch_String_array(5);
		
		sc.close();
	}
	
	// PASS
	public static void testPut1() {
		CustomTests obj = new CustomTests();
		
		for (int i = 0; i < 5; i++) {
			obj = new CustomTests();
			obj.f = new CustomTests();
		}
		
		/* must be a weak update */
		obj.f = new CustomTests();
	}
	
	// PASS
	public static void testPut2() {
		CustomTests obj = new CustomTests();
		obj.f = new CustomTests();
		obj.f.f = new CustomTests();
		obj.f.f.f = new CustomTests();
	}
	
	// PASS
	public static void testGet1() {
		CustomTests src1 = new CustomTests();
		CustomTests src2 = new CustomTests();
		
		src1.f = new CustomTests();
		src2.f = new CustomTests();
		
		/* Becomes 2 variables in SSA, but oh well */
		CustomTests target = src1.f;
		target = src2.f;
		
		System.out.println(target);
	}
	
	// PASS
	public static void testGet2() {
		CustomTests src1 = new CustomTests();
		CustomTests src2 = new CustomTests();
		
		src1.f = new CustomTests();
		src2.f = new CustomTests();
		
		CustomTests target = new CustomTests();
		
		/* Becomes a weak update, but oh well */
		target.f = src1.f;
		target.f = src2.f;
		
		System.out.println(target.f);
	}
	
	// PASS
	public static void testPhi1(int n) {
		CustomTests obj;
		
		if (n == 1) {
			obj = new CustomTests();
		}
		else {
			obj = new CustomTests();
		}

		/* Join node */
		System.out.println(obj);
	}
	
	// PASS
	public static void testPhi2(int n) {
		CustomTests obj = new CustomTests();
		
		if (n == 1) {
			obj.f = new CustomTests();
		}
		else {
			obj.f = new CustomTests();
		}
		
		/* Join node */
		System.out.println(obj.f);
	}
	
	// PASS
	public static void testPhi3(int n) {
		CustomTests obj;
		obj = new CustomTests();
		
		/* Weak update */
		while (n >= 1) {
			obj = new CustomTests();
			n--;
		}
		
		System.out.println(obj);
	}
	
	// PASS
	public static void testPhi4(int n) {
		CustomTests obj = new CustomTests();
		
		while (n >= 1) {
			obj.f = new CustomTests();
			n--;
		}

		/* Weak update */
		obj.f = new CustomTests();
		System.out.println(obj.f);
	}
	
	// PASS
	@SuppressWarnings("unused")
	public static void testNull() {
		CustomTests obj = null;
		
		if (obj != null) {
			/* Should be $\bot$ */
			obj = new CustomTests();
		}
		else {
			/* Should be added */
			obj = new CustomTests();
		}
	}
	
	// PASS
	@SuppressWarnings("unused")
	public static void testNull2() {
		CustomTests obj1 = null;
		CustomTests obj2 = null;
		
		if (obj1 != obj2) {
			/* Should be $\bot$ */
			obj1 = new CustomTests();
		}
		else {
			/* Should be added */
			obj2 = new CustomTests();
		}
	}
	
	// PASS
	public static void testNull3(boolean toss) {
		CustomTests obj = new CustomTests();
		
		if (toss) {
			obj = null;
		}
		
		System.out.println(obj);
	}
	
	// PASS
	public static void testNull4() {
		CustomTests obj = new CustomTests();
		obj.f = null;
		CustomTests obj2 = obj.f;
		System.out.println(obj2);
	}
	
	// PASS
	public static void testNull5() {
		CustomTests[] obj = new CustomTests[5];
		obj[2] = null;
		obj[0] = new CustomTests();
		CustomTests obj2 = obj[0];
		System.out.println(obj2);
	}
	
	// PASS
	public static void testPutStatic() {
		g = new CustomTests();
		g.f = new CustomTests();
	}
	
	// PASS
	public static void testGetStatic() {
		g = new CustomTests();
		
		@SuppressWarnings("unused")
		CustomTests ct = g;
	}
	
	// PASS
	public static void testCond1(int n, int m) {
		CustomTests obj1 = new CustomTests();
		CustomTests obj2 = new CustomTests();
		
		for (int i = 0; i < n; i++) {
			obj1.f = new CustomTests();
		}
		
		for (int i = 0; i < m; i++) {
			obj2.f = new CustomTests();
		}
		
		if (obj1.f == obj2.f) {
			// should be $\bot$
			obj1.f = new CustomTests();
		}
		else {
			// should be added to set
			obj2.f = new CustomTests();
		}
	}
	
	// PASS
	public static void testCond2(int n, int m) {
		CustomTests obj1 = new CustomTests();
		CustomTests obj2 = new CustomTests();
		
		for (int i = 0; i < n; i++) {
			obj1.f = new CustomTests();
		}
		
		for (int i = 0; i < m; i++) {
			obj2.f = new CustomTests();
		}
		
		if (obj1.f != obj2.f) {
			// should be added to set
			obj1.f = new CustomTests();
		}
		else {
			// should be $\bot$
			obj2.f = new CustomTests();
		}
	}
	
	// PASS
	public static void testCond3(int n, int m) {
		CustomTests obj1 = new CustomTests();
		CustomTests obj2 = new CustomTests();
		CustomTests obj3 = new CustomTests();
		
		for (int i = 0; i < n; i++) {
			obj1.f = new CustomTests();
			obj2.f = obj1.f;
		}
		
		for (int i = 0; i < m; i++) {
			obj1.f = new CustomTests();
			obj2.f = new CustomTests();
		}
		
		if (obj1.f == obj2.f) {
			obj3.f = obj1.f;
			obj3.f = obj2.f;
		}
		else {
			obj3.f = obj1.f;
			obj3.f = obj2.f;
		}
	}

	// PASS
	public static void testCond4(int n, int m) {
		CustomTests obj1 = new CustomTests();
		CustomTests obj2 = new CustomTests();
		CustomTests obj3 = new CustomTests();
		
		for (int i = 0; i < n; i++) {
			obj1.f = new CustomTests();
			obj2.f = obj1.f;
		}
		
		for (int i = 0; i < m; i++) {
			obj1.f = new CustomTests();
			obj2.f = new CustomTests();
		}
		
		if (obj1.f != obj2.f) {
			obj3.f = obj1.f;
			obj3.f = obj2.f;
		}
		else {
			obj3.f = obj1.f;
			obj3.f = obj2.f;
		}
	}
	
	public static void testCast() {
		Set<Integer> test = new HashSet<Integer>();
		test.add(1);
		HashSet<Integer> test2 = (HashSet<Integer>) test;
		
		System.out.println(test2);
	}
}
