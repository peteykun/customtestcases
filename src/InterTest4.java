
public class InterTest4 {
	public static void method1(int a, Integer b, int c) {
		
	}
	
	public static void method2(Integer a, int b, Integer c) {
		
	}
	
	public static int giveInt() {
		return 1;
	}
	
	public static Integer giveInteger() {
		return new Integer(1);
	}
	
	public static void main(String[] args) {
		int val1 = giveInt();
		Integer val2 = giveInteger();
		
		method1(val1, val2, val1);
		method2(val2, val1, val2);
	}
}
