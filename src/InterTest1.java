
public class InterTest1 {
	public static Object source() {
		Object obj = new Object();
		return obj;
	}
	
	public static void sink(Object o) {
		
	}
	
	public static void fetchAndPut() {
		Object obj;
		/* There will be 2 call strings here */
		obj = source();
		sink(obj);
	}
	
	public static void main(String[] args) {
		fetchAndPut();
		fetchAndPut();
	}
}
