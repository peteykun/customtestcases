
public class InterTest5 {
	public static Integer val;
	
	public static void method1() {
		val = new Integer(1);
		Integer temp = val;
		method2();
		val = temp;
	}
	
	public static void method2() {
		val = new Integer(2);
	}
	
	public static void main(String[] args) {
		method1();
	}
}
