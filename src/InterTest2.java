
public class InterTest2 {
	public static int source() {
		return 1;
	}
	
	public static void sink(int i) {
		
	}
	
	public static void fetchAndPut() {
		int obj;
		/* There will be 2 call strings here */
		obj = source();
		sink(obj);
	}
	
	public static void main(String[] args) {
		fetchAndPut();
		fetchAndPut();
	}
}
